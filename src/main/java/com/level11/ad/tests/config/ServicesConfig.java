package com.level11.ad.tests.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("classpath:application.yml")
@ConfigurationProperties("services")
public class ServicesConfig {
    @Value("${services.token}")
    public String token;

    @Value("${services.profile.entity.protocol:http}")
    public String profileEntityProtocol;

    @Value("${services.profile.entity.host:localhost}")
    public String profileEntityHost;

    @Value("${services.profile.entity.port}")
    public int profileEntityPort;

    @Value("${services.profile.task.protocol:http}")
    public String profileTaskProtocol;

    @Value("${services.profile.task.host:localhost}")
    public String profileTaskHost;

    @Value("${services.profile.task.port}")
    public int profileTaskPort;

    @Value("${services.folio.entity.protocol:http}")
    public String folioEntityProtocol;

    @Value("${services.folio.entity.host:localhost}")
    public String folioEntityHost;

    @Value("${services.folio.entity.port}")
    public int folioEntityPort;

    @Value("${services.folio.task.protocol:http}")
    public String folioTaskProtocol;

    @Value("${services.folio.task.host:localhost}")
    public String folioTaskHost;

    @Value("${services.folio.task.port}")
    public int folioTaskPort;

    private ServiceConfig profileEntity;

    private ServiceConfig profileTask;

    private ServiceConfig folioEntity;

    private ServiceConfig folioTask;

    public String token() {
        return token;
    }

    public ServiceConfig profileEntity() {
        if(profileEntity == null) {
            profileEntity = new ServiceConfig(profileEntityProtocol, profileEntityHost, profileEntityPort);
        }
        return profileEntity;
    }

    public ServiceConfig profileTask() {
        if(profileTask == null) {
            profileTask = new ServiceConfig(profileTaskProtocol, profileTaskHost, profileTaskPort);
        }
        return profileTask;
    }

    public ServiceConfig folioEntity() {
        if(folioEntity == null) {
            folioEntity = new ServiceConfig(folioEntityProtocol, folioEntityHost, folioEntityPort);
        }
        return folioEntity;
    }

    public ServiceConfig folioTask() {
        if(folioTask == null) {
            folioTask = new ServiceConfig(folioTaskProtocol, folioTaskHost, folioTaskPort);
        }
        return folioTask;
    }
}
