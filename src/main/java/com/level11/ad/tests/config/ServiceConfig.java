package com.level11.ad.tests.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class ServiceConfig {
    private String protocol;

    private String host;

    private int port;

    public ServiceConfig() {
    }

    @Autowired
    public ServiceConfig(String protocol, String host, int port) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;
    }

    public String protocol() {
        return protocol;
    }

    public String host() {
        return host;
    }

    public int port() {
        return port;
    }

    @Override
    public String toString() {
        return "ServiceConfig{" + "protocol='" + protocol + '\'' + ", host='" + host + '\'' + ", port=" + port + '}';
    }
}
