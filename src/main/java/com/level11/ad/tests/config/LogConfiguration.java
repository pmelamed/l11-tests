package com.level11.ad.tests.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogConfiguration {
    @Value("${auth.token}")
    private String token;

    public String getToken() {
        return token;
    }
}
