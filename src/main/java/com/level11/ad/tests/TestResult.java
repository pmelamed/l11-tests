package com.level11.ad.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.io.IOException;

@Slf4j
public class TestResult {
    private ResponseEntity<String> response;
    private ObjectMapper mapper;

    public TestResult(ResponseEntity<String> response, ObjectMapper mapper) {
        this.response = response;
        this.mapper = mapper;
    }

    public HttpStatus status() {
        return response.getStatusCode();
    }

    public TestResult checkStatus(HttpStatus... statuses) {
        for(HttpStatus status: statuses) {
            if(status().equals(status)) {
                return this;
            }
        }
        throw new IllegalStateException("HTTP code doesn't match: code = " + status());
    }

    public TestResult checkOk() {
        return checkStatus(HttpStatus.OK);
    }

    public String response() {
        return response.getBody();
    }
    
    public <T> T response(Class<T> clazz) {
        try {
            return mapper.readValue(response.getBody(), clazz);
        } catch (IOException e) {
            log.error("Can't parse response:\n" + response.getBody(), e);
            return null;
        }
    }
}
