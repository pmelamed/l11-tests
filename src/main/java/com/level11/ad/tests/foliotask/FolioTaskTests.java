package com.level11.ad.tests.foliotask;

import com.level11.ad.tests.TestClient;
import com.level11.ad.tests.config.ServicesConfig;
import com.level11.o8.folio.model.band.BandCreate;
import com.level11.o8.folio.model.band.BandResource;
import com.level11.o8.folio.model.band.BandUpdate;
import com.level11.o8.folio.model.charge.ChargeResource;
import com.level11.o8.folio.model.customer.CustomerResource;
import com.level11.o8.folio.model.purchase.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

@Slf4j
public class FolioTaskTests {
  
  private TestClient folioTask;
  private TestClient profileTask;
  
  public FolioTaskTests(ServicesConfig services) {
    this.folioTask = new TestClient().withService(services.folioTask()).withToken(services.token());
    this.profileTask = new TestClient().withService(services.profileTask()).withToken(services.token());
  }
  
  public void scenario1() {
    profileTask.call(HttpMethod.POST, "/v1/me");
    
    folioTask.withDelay(20);
    
    PaymentParams paymentParams1 =
        new PaymentParams("4242424242424242", "111", 12, 2019, "JOHN DOE", "USD", "Artyoma str 86", "apt 21",
            "Donetsk", "DN", "UA", "83001", "Visa Donetsk"
        );
    PaymentMethod method1 =
        folioTask.call(HttpMethod.POST, "/v1/payment", paymentParams1).checkOk().response(PaymentMethod.class);
    
    PaymentParams paymentParams2 =
        new PaymentParams("5555555555554444", "555", 10, 2024, "JOHN DOE JR", "USD", "Timoshenko str 2B",
            "apt 145", "Kyiv", "KY", "UA", "04001", "MasterCard Kyiv"
        );
    PaymentMethod method2 =
        folioTask.call(HttpMethod.POST, "/v1/payment", paymentParams2).checkOk().response(PaymentMethod.class);
    
    PaymentMethodList methodsList =
        folioTask.call(HttpMethod.GET, "/v1/payment").checkOk().response(PaymentMethodList.class);
    
    ChargeParams chargeParams1 = new ChargeParams(9900, "USD", "AD1 PRE_AUTH CHARGE", "VENDOR1", false);
    ChargeResult charge1 =
        folioTask.call(HttpMethod.POST, "/v1/payment/" + method1.getId() + "/charge", chargeParams1).checkOk()
                 .response(ChargeResult.class);
    ChargeParams chargeParams2 = new ChargeParams(10001, "USD", "AD1 CAPTURE CHARGE", "VENDOR1", true);
    ChargeResult charge2 =
        folioTask.call(HttpMethod.POST, "/v1/payment/" + method1.getId() + "/charge", chargeParams2).checkOk()
                 .response(ChargeResult.class);
    ChargeParams chargeParams3 = new ChargeParams(1999, "USD", "AD2 PRE_AUTH CHARGE", "VENDOR1", false);
    ChargeResult charge3 =
        folioTask.call(HttpMethod.POST, "/v1/payment/" + method2.getId() + "/charge", chargeParams3).checkOk()
                 .response(ChargeResult.class);
    ChargeParams chargeParams4 = new ChargeParams(2001, "USD", "AD2 CAPTURE CHARGE", "VENDOR1", true);
    ChargeResult charge4 =
        folioTask.call(HttpMethod.POST, "/v1/payment/" + method2.getId() + "/charge", chargeParams4).checkOk()
                 .response(ChargeResult.class);
    
    ChargeResource capture1 = folioTask.call(HttpMethod.POST, "/v1/charge/" + charge1.getResource().getId() + "/capture").checkOk()
                                       .response(ChargeResource.class);
    
    RefundResult refund1 = folioTask.call(HttpMethod.POST, "/v1/charge/" + charge1.getResource().getId() + "/refund").checkOk()
                                    .response(RefundResult.class);
    RefundResult refund2 = folioTask.call(HttpMethod.POST, "/v1/charge/" + charge2.getResource().getId() + "/refund").checkOk()
                                    .response(RefundResult.class);
    RefundResult refund3 = folioTask.call(HttpMethod.POST, "/v1/charge/" + charge3.getResource().getId() + "/refund").checkOk()
                                    .response(RefundResult.class);
    RefundResult refund4 = folioTask.call(HttpMethod.POST, "/v1/charge/" + charge4.getResource().getId() + "/refund").checkOk()
                                    .response(RefundResult.class);
    
    folioTask.call(HttpMethod.DELETE, "/v1/customer", null).checkOk();
  }
  
  public void scenarioDefaultPayment() {
    try {
      // profileTask.call(HttpMethod.POST, "/v1/person/me");
      folioTask.withDelay(10);
      
      PaymentParams paymentParams1 =
          new PaymentParams("4242424242424242", "111", 12, 2019, "JOHN DOE", "USD", "Artyoma str 86", "apt 21",
              "Donetsk", "DN", "UA", "83001", "Visa Donetsk"
          );
      PaymentMethod method1 = folioTask.call(HttpMethod.POST, "/v1/payment", paymentParams1).checkOk().response(PaymentMethod.class);
      PaymentParams paymentParams2 =
          new PaymentParams("5555555555554444", "555", 10, 2024, "JOHN DOE JR", "USD", "Timoshenko str 2B",
              "apt 145", "Kyiv", "KY", "UA", "04001", "MasterCard Kyiv"
          );
      
      PaymentMethod method2 = folioTask.call(HttpMethod.POST, "/v1/payment", paymentParams2).checkOk().response(PaymentMethod.class);
      
      PaymentMethodList methodsList =
          folioTask.call(HttpMethod.GET, "/v1/payment").checkOk().response(PaymentMethodList.class);
      
      CustomerResource customer1 =
          folioTask.call(HttpMethod.PUT, "/v1/payment/" + method1.getId() + "/set-default").checkOk().response(CustomerResource.class);
      Assertions.assertEquals(customer1.getPrimaryPayment(), Long.valueOf(method1.getId()));
      
      customer1 =
          folioTask.call(HttpMethod.GET, "/v1/customer").checkOk().response(CustomerResource.class);
      Assertions.assertEquals(customer1.getPrimaryPayment(), Long.valueOf(method1.getId()));
      
      customer1 =
          folioTask.call(HttpMethod.PUT, "/v1/payment/" + method2.getId() + "/set-default").checkOk().response(CustomerResource.class);
      Assertions.assertEquals(customer1.getPrimaryPayment(), Long.valueOf(method2.getId()));
      
      customer1 =
          folioTask.call(HttpMethod.GET, "/v1/customer").checkOk().response(CustomerResource.class);
      Assertions.assertEquals(customer1.getPrimaryPayment(), Long.valueOf(method2.getId()));
    }
    finally {
      folioTask.call(HttpMethod.DELETE, "/v1/customer").checkOk();
    }
  }
  
  public void scenarioBand() {
    folioTask.withDelay(10);
    
    PaymentParams paymentParams1 =
        new PaymentParams("4242424242424242", "111", 12, 2019, "JOHN DOE", "USD", "Artyoma str 86", "apt 21",
            "Donetsk", "DN", "UA", "83001", "Visa Donetsk"
        );
    PaymentMethod method1 = folioTask.call(HttpMethod.POST, "/v1/payment", paymentParams1).checkOk().response(PaymentMethod.class);
    PaymentParams paymentParams2 =
        new PaymentParams("5555555555554444", "555", 10, 2024, "JOHN DOE JR", "USD", "Timoshenko str 2B",
            "apt 145", "Kyiv", "KY", "UA", "04001", "MasterCard Kyiv"
        );
    
    PaymentMethod method2 = folioTask.call(HttpMethod.POST, "/v1/payment", paymentParams2).checkOk().response(PaymentMethod.class);
    
    BandParams bandParams1 = new BandParams("AAA1-BBBB-CCCC-DDDDEEEE", "Band#1", null, "e-mail@email.com", null);
    BandResource band1 = folioTask.call(HttpMethod.POST,
        "/v1/band",
        bandParams1
    ).checkOk().response(
        BandResource.class);
    Assertions.assertEquals(band1.getBandGuid(), bandParams1.getMediaUid());
    Assertions.assertEquals(band1.getNickname(), bandParams1.getNickname());
    
    BandResource band1r = folioTask.call(HttpMethod.GET,
        "/v1/band/" + band1.getId()
    ).checkOk().response(
        BandResource.class);
    Assertions.assertEquals(band1r.getId(), band1.getId());
    
    folioTask.call(HttpMethod.POST,
        "/v1/band",
        new BandParams("AAA1-BBBB-CCCC-DDDDEEEE", "Band#1.1", method1.getId(), "e-mail@email.com", null)
    ).checkStatus(HttpStatus.CONFLICT);
    
    folioTask.call(HttpMethod.POST,
        "/v1/band",
        new BandParams("AAA0-BBBB-CCCC-DDDDEEEE", "Band#1", method1.getId(), "e-mail@email.com", null)
    ).checkStatus(HttpStatus.CONFLICT);
    
    BandParams bandParams2 = new BandParams("AAA2-BBBB-CCCC-DDDDEEEE", "Band#2", method1.getId(), "e-mail@email.com", null);
    BandResource band2 = folioTask.call(HttpMethod.POST,
        "/v1/band",
        bandParams2
    ).checkOk().response(
        BandResource.class);
    Assertions.assertEquals(band2.getBandGuid(), bandParams2.getMediaUid());
    Assertions.assertEquals(band2.getNickname(), bandParams2.getNickname());
    Assertions.assertEquals(band2.getPaymentMethod(), bandParams2.getPaymentMethod());
    
    folioTask.call(HttpMethod.PUT,
        "/v1/band/" + band2.getId(),
        new BandUpdate("Band#1", "e-mail@email.com", null, null, null, null, null)
    ).checkStatus(HttpStatus.CONFLICT);
    
    BandUpdate bandUpdate2 = new BandUpdate("Band#3", null, "+1234567890", null, null, method2.getId(), null);
    BandResource band2u = folioTask.call(HttpMethod.PUT,
        "/v1/band/" + band2.getId(),
        bandUpdate2
    ).checkOk().response(
        BandResource.class);
    Assertions.assertEquals(band2u.getNickname(), bandUpdate2.getNickname());
    Assertions.assertEquals(band2u.getEmail(), band2.getEmail());
    Assertions.assertEquals(band2u.getPhone(), bandUpdate2.getPhone());
    Assertions.assertEquals(band2u.getPaymentMethod(), bandUpdate2.getPaymentMethod());
    
    band1r = folioTask.call(HttpMethod.GET,
        "/v1/band/media-uid/" + band1.getBandGuid()
    ).checkOk().response(
        BandResource.class);
    Assertions.assertEquals(band1r.getId(), band1.getId());
    
    BandStatus status1 = folioTask.call(HttpMethod.GET,
        "/v1/band/" + band1.getId() + "/status"
    ).checkOk().response(BandStatus.class);
    Assertions.assertEquals(status1.isChargeable(), false);
    Assertions.assertEquals(status1.isLost(), false);
    Assertions.assertEquals(status1.isPaymentAssigned(), false);
    
    status1 = folioTask.call(HttpMethod.GET,
        "/v1/ttp/" + band1.getBandGuid() + "/status"
    ).checkOk().response(BandStatus.class);
    Assertions.assertEquals(status1.isChargeable(), false);
    Assertions.assertEquals(status1.isLost(), false);
    Assertions.assertEquals(status1.isPaymentAssigned(), false);
  
    ChargeParams charge1 = new ChargeParams(9999, "USD", "CHRG1", "VNDR1", true);
    folioTask.call(HttpMethod.POST, "/v1/ttp/" + band1.getBandGuid() + "/charge", charge1).checkStatus(HttpStatus.BAD_REQUEST);
    
    folioTask.call(HttpMethod.PUT,
        "/v1/band/" + band1.getId(),
        new BandUpdate(null, null, null, true, null, method1.getId(), null)
    ).checkOk();
    
    status1 = folioTask.call(HttpMethod.GET,
        "/v1/band/" + band1.getId() + "/status"
    ).checkOk().response(BandStatus.class);
    Assertions.assertEquals(status1.isChargeable(), false);
    Assertions.assertEquals(status1.isLost(), true);
    Assertions.assertEquals(status1.isActive(), true);
    Assertions.assertEquals(status1.isPaymentAssigned(), true);
    
    folioTask.call(HttpMethod.PUT,
        "/v1/band/" + band1.getId(),
        new BandUpdate(null, null, null, false, true, null, null)
    ).checkOk();
    
    status1 = folioTask.call(HttpMethod.GET,
        "/v1/band/" + band1.getId() + "/status"
    ).checkOk().response(BandStatus.class);
    Assertions.assertEquals(status1.isChargeable(), false);
    Assertions.assertEquals(status1.isLost(), false);
    Assertions.assertEquals(status1.isActive(), false);
    Assertions.assertEquals(status1.isPaymentAssigned(), true);
    
    folioTask.call(HttpMethod.PUT,
        "/v1/band/" + band1.getId(),
        new BandUpdate(null, null, null, false, false, null, null)
    ).checkOk();
    
    status1 = folioTask.call(HttpMethod.GET,
        "/v1/band/" + band1.getId() + "/status"
    ).checkOk().response(BandStatus.class);
    Assertions.assertEquals(status1.isChargeable(), true);
    Assertions.assertEquals(status1.isLost(), false);
    Assertions.assertEquals(status1.isActive(), true);
    Assertions.assertEquals(status1.isPaymentAssigned(), true);
    
    ChargeResult charge1r =
        folioTask.call(HttpMethod.POST, "/v1/ttp/" + band1.getBandGuid() + "/charge", charge1)
                 .checkOk().response(ChargeResult.class);
    Assertions.assertNotNull(charge1r.getResource().getId());
    Assertions.assertEquals(charge1r.getResource().getBandMediaUid(), band1.getBandGuid());
    Assertions.assertEquals(charge1r.getResource().getBandNickname(), band1.getNickname());
    Assertions.assertEquals(charge1r.getResource().getPaymentId(), Long.valueOf(method1.getId()));
  }
}
