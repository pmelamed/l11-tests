package com.level11.ad.tests;

import com.level11.ad.tests.config.LogConfiguration;
import com.level11.ad.tests.config.ServicesConfig;
import com.level11.ad.tests.foliotask.FolioTaskTests;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@Slf4j
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class ApplicationMain implements CommandLineRunner {
    @Autowired
    private LogConfiguration config;

    @Autowired
    private ServicesConfig services;

    public static void main(String[] args) {
        SpringApplication.run(ApplicationMain.class, args);
    }

    @Override
    public void run(String... args) {
        log.debug("Hello, "+ config.getToken() + "!");
        try {
            new FolioTaskTests(services).scenarioBand();
        } catch(Throwable e) {
            log.error("Test failed", e);
        }
    }
}
