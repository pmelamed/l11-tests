package com.level11.ad.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.level11.ad.tests.config.ServiceConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class TestClient {
    private RestTemplate template;

    private String token = null;

    private int port = 9999;

    private String protocol = "http";

    private String host = "localhost";

    private boolean logRequest = true;

    private boolean logResponse = true;

    private ObjectMapper mapper;
    
    private int delay = 0;

    public TestClient() {
        this.template = new RestTemplate();
        this.mapper = new ObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                                        .enable(SerializationFeature.INDENT_OUTPUT).findAndRegisterModules();
    }

    public TestClient withService(ServiceConfig config) {
        log.info("Configure client with " + config.toString());
        return withProtocol(config.protocol()).withHost(config.host()).withPort(config.port());
    }

    public TestClient withToken(String token) {
        this.token = token;
        return this;
    }

    public TestClient withProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public TestClient withHost(String host) {
        this.host = host;
        return this;
    }

    public TestClient withPort(int port) {
        this.port = port;
        return this;
    }

    public TestClient withDelay(int delay) {
        this.delay = delay;
        return this;
    }

    private void doLogRequest(HttpMethod method, String url, HttpEntity<String> call) {
        if (!logRequest) {
            return;
        }
        StringBuilder result = new StringBuilder("\nRequest:\n---------------------------------------------\n");
        result.append(method.name()).append(" ").append(url).append("\n");
        HttpHeaders headers = call.getHeaders();
        if (!headers.isEmpty()) {
            headers.forEach((n, v) -> {
                result.append(n).append(":").append(v).append("\n");
            });
        }
        if (call.hasBody()) {
            result.append("---------------------------------------------\n");
            result.append(call.getBody());
            result.append("\n---------------------------------------------\n");
        }
        log.info(result.toString());
    }

    private void doLogResponse(ResponseEntity<String> response) {
        if (!logResponse) {
            return;
        }
        StringBuilder result = new StringBuilder("\nResponse:\n---------------------------------------------\n");
        result.append(response.getStatusCode().value()).append(" ").append(response.getStatusCode().name()).append("\n");
        HttpHeaders headers = response.getHeaders();
        if (!headers.isEmpty()) {
            headers.forEach((n, v) -> {
                result.append(n).append(":").append(v).append("\n");
            });
        }
        if (response.hasBody()) {
            result.append("---------------------------------------------\n");
            result.append(response.getBody());
            result.append("\n---------------------------------------------\n");
        }
        log.info(result.toString());
    }

    public TestResult call(HttpMethod method, String url) {
        return call(method, url, null);
    }

    public TestResult call(HttpMethod method, String url, Object content) {
        HttpHeaders headers = new HttpHeaders();
        if (token != null) {
            headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        }
        if (content != null) {
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        }

        String payload = null;
        try {
            if (content != null) {
                payload = mapper.writeValueAsString(content);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Couldn't stringify payload", e);
        }
        HttpEntity<String> entity = new HttpEntity<>(payload, headers);

        String fullUrl = protocol + "://" + host + ":" + port + url;
        
        if(logResponse || logRequest) {
            log.info("\n\n\n\n");
        }
        doLogRequest(method, fullUrl, entity);
        ResponseEntity<String> response;
        try {
            response = template.exchange(fullUrl, method, entity, String.class);
        } catch(RestClientResponseException e) {
            response = ResponseEntity.status(e.getRawStatusCode()).body(e.getResponseBodyAsString());
        }
        doLogResponse(response);
        try {
            if(delay > 0) {
                for(int i = delay; i > 0; --i) {
                    System.out.print(String.format("%2d", i));
                    Thread.sleep(1000);
                    System.out.print("\r");
                }
                System.out.print("\r");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return new TestResult(response, mapper);
    }
}
